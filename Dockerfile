ARG PHP_IMAGE_VERSION="7.2"
FROM registry.gitlab.com/yroot/docker/php-fpm-nginx:php${PHP_IMAGE_VERSION}

ARG S6_VERSION="1.21.4.0"

ENV FLOW_REWRITEURLS 1
ENV S6_BEHAVIOUR_IF_STAGE2_FAILS 2

# Set default values for env vars used in init scripts, override them if needed
ENV DB_DATABASE db
ENV DB_HOST db
ENV DB_USER admin
ENV DB_PASS pass

# Basic build-time metadata as defined at http://label-schema.org
LABEL org.label-schema.docker.dockerfile="/Dockerfile" \
	org.label-schema.license="MIT" \
	org.label-schema.name="Neos Alpine Docker Image" \
	org.label-schema.url="https://gitlab.com/yroot/docker/neos-alpine" \
	org.label-schema.vcs-url="https://gitlab.com/yroot/docker/neos-alpine.git" \
	org.label-schema.vcs-type="Git"

RUN set -x \
    && apk update \
    && apk add --no-cache sed s6 mysql-client \
    && rm -rf /var/cache/apk/*
    
COPY root /
    
ADD https://github.com/just-containers/s6-overlay/releases/download/v${S6_VERSION}/s6-overlay-amd64.tar.gz /tmp/
RUN tar xzf /tmp/s6-overlay-amd64.tar.gz -C / && rm /tmp/s6-overlay-amd64.tar.gz \
    && echo "date.timezone=${PHP_TIMEZONE:-UTC}" > $PHP_INI_DIR/conf.d/date_timezone.ini \
    && echo "memory_limit=${PHP_MEMORY_LIMIT:-2048M}" > $PHP_INI_DIR/conf.d/memory_limit.ini \
    && echo "upload_max_filesize=${PHP_UPLOAD_MAX_FILESIZE:-512M}" > $PHP_INI_DIR/conf.d/upload_max_filesize.ini \
    && echo "post_max_size=${PHP_UPLOAD_MAX_FILESIZE:-512M}" > $PHP_INI_DIR/conf.d/post_max_size.ini \
    && echo "allow_url_include=${PHP_ALLOW_URL_INCLUDE:-1}" > $PHP_INI_DIR/conf.d/allow_url_include.ini \
    && echo "max_execution_time=${PHP_MAX_EXECUTION_TIME:-240}" > $PHP_INI_DIR/conf.d/max_execution_time.ini \
    && echo "max_input_vars=${PHP_MAX_INPUT_VARS:-1500}" > $PHP_INI_DIR/conf.d/max_input_vars.ini \
    && echo "yaml.decode_php=0" > $PHP_INI_DIR/conf.d/yaml_decode_php.ini \
    && echo "expose_php=Off" > $PHP_INI_DIR/conf.d/expose_php.ini \
    && sed -i -e "s#listen = 9000#listen = /var/run/php-fpm.sock#" /usr/local/etc/php-fpm.d/zz-docker.conf \
    && sed -i -e "s#;clear_env = no#clear_env = no#" /usr/local/etc/php-fpm.d/www.conf \ 
    && deluser www-data \
    && delgroup cdrw \
    && addgroup -g 80 www-data \
    && adduser -u 80 -G www-data -s /bin/bash -D www-data -h /data \
    && rm -Rf /home/www-data \
    && echo "listen.owner = www-data" >> /usr/local/etc/php-fpm.d/zz-docker.conf \
    && echo "listen.group = www-data" >> /usr/local/etc/php-fpm.d/zz-docker.conf \
    && echo "listen.mode = 0660" >> /usr/local/etc/php-fpm.d/zz-docker.conf \
    && chmod +x /provision-neos.sh \
    && chmod +x /etc/cont-init.d/* \
    && chmod +x /etc/services.d/*/run \
    && mkdir -p /data/www \
    && mkdir -p /data/logs \
    && mkdir -p /data/tmp/nginx \
    #&& chown 80:80 -Rf /var/lib/nginx \
    && chown 80:80 -Rf /data/

VOLUME /data

# Expose ports
EXPOSE 80

# Define working directory
WORKDIR /data/www

# Define entrypoint and command
ENTRYPOINT ["/init"]
    