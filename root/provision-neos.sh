#!/usr/bin/env bash
set -ex


mkdir -p /data/logs
mkdir -p /data/tmp/nginx

# Provision conainer at first run
if [ -f /data/www/composer.json ]
then
    if [ ! -f /data/www/Configuration/Settings.yaml ];
    then 
        # DB connection settings
        mkdir -p /data/www/Configuration
        cp /Settings.yaml /data/www/Configuration/
        
        sed -i -e "s#%env:MYSQL_DATABASE%#${MYSQL_DATABASE}#" /data/www/Configuration/Settings.yaml
        sed -i -e "s#%env:MYSQL_USER%#${MYSQL_USER}#" /data/www/Configuration/Settings.yaml
        sed -i -e "s#%env:MYSQL_PASSWORD%#${MYSQL_PASSWORD}#" /data/www/Configuration/Settings.yaml
        sed -i -e "s#%env:MYSQL_HOST%#${MYSQL_HOST}#" /data/www/Configuration/Settings.yaml
    fi
    
    # Set permissions
    chown www-data:www-data -R /tmp/
    chown www-data:www-data -R /data/
    chmod g+rwx -R /data/
else
    echo "composer.json not found!"
    exit 1
fi